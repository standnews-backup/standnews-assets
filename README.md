# About
Lightweight express.js code to serve as the stand news assets server. <br>
Images are downloaded on-demand from the internet archive.

# Deploying
```
npm install
```
```
node server.js
```
The app will start at localhost:2998.
